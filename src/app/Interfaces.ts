export interface SelectOption {
  id: string;
  value: string;
}

export interface Question {
  id: number;
  type: 'select' | 'text';
  text: string;
  selectOptions?: SelectOption[];
}

export interface Answer {
  questionId: number;
  id: number;
  text?: string;
  selectOptions?: {id: string, value: boolean}[];
}
