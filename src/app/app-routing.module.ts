import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { BuilderComponent } from './builder/builder.component';
import { AnswersComponent } from './answers/answers.component';

const routes: Routes = [
  { path: 'form/builder', component: BuilderComponent },
  { path: 'form/answers', component: AnswersComponent },
  { path: '', redirectTo: 'form/builder', pathMatch: 'full' }, // redirect
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes), CommonModule],
  exports: [RouterModule],
})
export class AppRoutingModule {}
