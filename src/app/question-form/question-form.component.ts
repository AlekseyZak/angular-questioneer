import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Question } from '../Interfaces';


@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.scss'],
})
export class QuestionFormComponent implements OnInit {
  constructor(@Inject(MAT_DIALOG_DATA) public data: Question[]) {
    this.question = JSON.parse(JSON.stringify(data));
  }
  public question: Question;
  questionTypes = [
    { value: 'select', viewValue: 'CheckBox List' },
    { value: 'text', viewValue: 'Paragraph' },
  ];

  public addAnswer(question: Question) {
    if (!question.selectOptions)
      throw new Error('Question does not have selectOptions');
    question.selectOptions.push({
      id: 'opt_' + question.selectOptions.length,
      value: '',
    });
  }

  ngOnInit(): void {}
}
