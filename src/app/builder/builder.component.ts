import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';
import { AnswersService } from '../answers.service';
import { Answer, Question } from '../Interfaces';
import { QuestionFormComponent } from '../question-form/question-form.component';
import { QuestionsService } from '../questions.service';

@Component({
  selector: 'app-builder',
  templateUrl: './builder.component.html',
  styleUrls: ['./builder.component.scss'],
})
export class BuilderComponent implements OnInit {
  ngOnInit(): void {}
  questions: Question[];
  answers: Answer[];
  form: FormGroup;

  constructor(
    public dialog: MatDialog,
    public fb: FormBuilder,
    public questionsService: QuestionsService,
    public answersService: AnswersService
  ) {
    this.questions = questionsService.getAll();
    this.answers = answersService.getAll();
    this.form = this.getForm();
  }

  getForm(): FormGroup {
    let fg = this.fb.group({});
    for (let a of this.answers) {
      if (a.text !== undefined) {
        fg.addControl(a.id.toString(), this.fb.control(''));
        fg.controls[a.id.toString()].valueChanges.subscribe((v) => {
          console.log('CHANGE', v)
        });
      }
      if (a.selectOptions) {
        a.selectOptions.forEach((opt) => {
          fg.addControl(`${a.id}|${opt.id}`, this.fb.control(false));
          fg.controls[`${a.id}|${opt.id}`].valueChanges.subscribe((v) => {
            console.log('CHANGE', v);
          });
        });
      }
    }
    return fg;
  }

  openDialog() {
    const dialogRef = this.dialog.open(QuestionFormComponent, {
      data: {
        id: Date.now(),
        type: 'select',
        text: 'Question text here',
        selectOptions: [],
      },
    });

    dialogRef.afterClosed().subscribe((result: Question) => {
      console.log(`Dialog result:`, result);
      this.questions.push(result);
      this.answers.push(this.answersService.getAnswer(result));
      this.form = this.getForm();
    });
  }
}
