import { Injectable } from '@angular/core';
import { Question } from './Interfaces';

@Injectable({
  providedIn: 'root',
})
export class QuestionsService {
  private questions: Question[];

  constructor() {
    const q1: Question = {
      id: Date.now(),
      type: 'text',
      text: 'Please tell us about yourself',
    };
    const q2: Question = {
      id: Date.now(),
      type: 'select',
      text: 'Please select the languages you know',
      selectOptions: [
        { id: 'opt_0', value: 'Javascript' },
        { id: 'opt_1', value: 'PHP' },
        { id: 'opt_2', value: 'Ruby' },
      ],
    };
    this.questions = [q1, q2];
  }

  getAll(): Question[] {
    return this.questions;
  }
}
