import { Injectable } from '@angular/core';
import { Answer, Question } from './Interfaces';
import { QuestionsService } from './questions.service';

@Injectable({
  providedIn: 'root',
})
export class AnswersService {
  private answers: Answer[];
  constructor(private questionsService: QuestionsService) {
    const [q1, q2] = this.questionsService.getAll();
    this.answers = [
      {
        id: Date.now(),
        questionId: q1.id,
        text: '',
      },
      {
        id: Date.now(),
        questionId: q2.id,
        selectOptions: [
          { id: 'opt_0', value: false },
          { id: 'opt_1', value: false },
          { id: 'opt_2', value: false },
        ],
      },
    ];
  }
  getAll(): Answer[] {
    return this.answers;
  }

  getAnswer(question: Question): Answer {
    let a = this.answers.find((a) => a.questionId === question.id);
    if (a) return a;

    if (!a) a = { id: Date.now(), questionId: question.id };
    if (question.type === 'text') {
      a.text = '';
    }
    if (question.type === 'select' && question.selectOptions) {
      a.selectOptions = [];
      question.selectOptions.forEach((opt) => {
        a?.selectOptions?.push({ id: opt.id, value: false });
      });
    }
    this.answers.push(a);
    return a;
  }
}
