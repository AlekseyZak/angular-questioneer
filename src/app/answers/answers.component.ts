import { Component, OnInit } from '@angular/core';
import { AnswersService } from '../answers.service';
import { Answer, Question } from '../Interfaces';
import { QuestionsService } from '../questions.service';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.scss'],
})
export class AnswersComponent implements OnInit {
  public answers: Answer[];
  public questions: Question[];
  constructor(
    public questionsService: QuestionsService,
    public answersService: AnswersService
  ) {
    this.questions = questionsService.getAll();
    this.answers = answersService.getAll();
  }

  ngOnInit(): void {}
}
